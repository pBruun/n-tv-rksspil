package game;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;
import java.util.List;
import java.util.Random;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Platform;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.util.Duration;

public class ClientThread extends Thread {

	private Socket clientSocket;
	private Player me;
	private List<Player> players;

	public ClientThread(Socket clientSocket, List<Player> players, Player me) {
		this.clientSocket = clientSocket;
		this.players = players;
		this.me = me;
	}

	@Override
	public void run() {
		boolean run = true;
		while (run) {
			try {
				BufferedReader inFromServer = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
				String str = inFromServer.readLine();
				System.out.println(str);
				String[] args = str.split(";");
				if (str.startsWith("ack;newplayer;")) {
					String[] p = args[2].split("!");
					me = new Player(p[0], Integer.parseInt(p[1]), Integer.parseInt(p[2]), Integer.parseInt(p[3]), p[4]);
					players.add(me);
					String[] persons = args[3].substring(0, args[3].length() - 1).split(":");
					for (String per : persons) {
						String[] perArgs = per.split("!");
						if (players.stream().noneMatch(f -> f.getName().equals(perArgs[0]))) {
							Player newPlayer = new Player(perArgs[0], Integer.parseInt(perArgs[1]),
									Integer.parseInt(perArgs[2]), Integer.parseInt(perArgs[3]), perArgs[4]);
							players.add(newPlayer);
							Platform.runLater(() -> {
								Main.fields[newPlayer.getXpos()][newPlayer.getYpos()]
										.setGraphic(new ImageView(Main.hero_up));
								Main.scoreList.setText(getScoreList());
							});
						}
					}
					Platform.runLater(() -> {
						Main.fields[me.getXpos()][me.getYpos()].setGraphic(new ImageView(Main.hero_up));
						Main.scoreList.setText(getScoreList());
					});
				} else if (str.startsWith("newplayer;")) {
					String[] p = args[1].split("!");
					Player addP = new Player(p[0], Integer.parseInt(p[1]), Integer.parseInt(p[2]),
							Integer.parseInt(p[3]), p[4]);
					players.add(addP);
					Platform.runLater(() -> {
						Main.fields[addP.getXpos()][addP.getYpos()].setGraphic(new ImageView(Main.hero_up));
						Main.scoreList.setText(getScoreList());
					});
				} else if (str.startsWith("playermove;")) {
					String[] p = args[1].split("!");
					Player player = this.players.stream().filter(f -> f.getName().equals(p[0])).findAny().orElse(null);
					int newX = Integer.parseInt(p[1]);
					int newY = Integer.parseInt(p[2]);
					String di = p[4];
					playerMoved(player, newX, newY, di);
					player.setPoint(Integer.parseInt(p[3]));
					Platform.runLater(() -> {
						Main.scoreList.setText(getScoreList());
					}); 
				} else if (str.startsWith("shot")) {
					String[] p = args[1].split("!");
					Player player = this.players.stream().filter(f -> f.getName().equals(p[0])).findAny().orElse(null);
					fireShot(player);

				} else if (str.startsWith("playerremoved;")) {
					String[] p = args[1].split("!");
					if (p[0].equals(me.getName())) {
						clientSocket.close();
						run = false;
					}
					removePlayer(p[0]);
					Platform.runLater(() -> {
						Main.fields[Integer.parseInt(p[1])][Integer.parseInt(p[2])]
								.setGraphic(new ImageView(Main.image_floor));
						Main.scoreList.setText(removePlayerFromScoreList(p[0]));
					}); 

				} else if (str.startsWith("hit;")) {
					String[] p = args[1].split("!");
					me.setPoint(Integer.parseInt(p[3]));
					Platform.runLater(() -> {
						Main.scoreList.setText(getScoreList());
					}); 
				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	public void fireShot(Player p) {
		String[] board = Main.getBoard();
		int[] xMove = { 0 };
		int[] yMove = { 0 };
		switch (p.getDirection()) {
		case "left":
			xMove[0] = -1;
			yMove[0] = 0;
			break;
		case "right":
			xMove[0] = 1;
			yMove[0] = 0;
			break;
		case "up":
			xMove[0] = 0;
			yMove[0] = -1;
			break;
		case "down":
			xMove[0] = 0;
			yMove[0] = 1;
			break;
		default:
			break;
		}
		Platform.runLater(() -> {
			int[] y = { p.getYpos() };
			int[] x = { p.getXpos() };
			int x_move = xMove[0];
			int y_move = yMove[0];
			boolean finished = false;
			Image wall = Main.fire_wall_south;
			Image hit = Main.fire_right;
			Image line = Main.fire_horizontal;
			if (x_move == 1) {
				wall = Main.fire_wall_east;
				hit = Main.fire_right;
			} else if (x_move == -1) {
				wall = Main.fire_wall_west;
				hit = Main.fire_left;
			} else if (y_move == 1) {
				wall = Main.fire_wall_south;
				hit = Main.fire_down;
				line = Main.fire_vertical;
			} else if (y_move == -1) {
				wall = Main.fire_wall_north;
				hit = Main.fire_up;
				line = Main.fire_vertical;
			}
			while (!finished) {
				if (board[y[0] + y_move].charAt(x[0] + x_move) == 'w') {
					finished = true;
				} else if (y[0] >= 19 || y[0] <= 1) {
					finished = true;
				} else if (players.stream().anyMatch(
						f -> !f.getName().equals(p.getName()) && f.getXpos() == x[0] && f.getYpos() == y[0])) {
					finished = true;
					Main.fields[x[0]-x_move][y[0]-y_move].setGraphic(new ImageView(hit));
					scheduleFloor(x[0]-x_move, y[0]-y_move);
				} else {
					y[0] = y[0] + y_move;
					x[0] = x[0] + x_move;
					if (board[y[0] + y_move].charAt(x[0] + x_move) == 'w') {
						Main.fields[x[0]][y[0]].setGraphic(new ImageView(wall));
						scheduleFloor(x[0], y[0]);
					} else {
						Main.fields[x[0]][y[0]].setGraphic(new ImageView(line));
						scheduleFloor(x[0], y[0]);
					}
				}
			}
		});

	}

	private void scheduleFloor(int x, int y) {
		new Timeline(new KeyFrame(Duration.millis(1000), ae -> {
			if (players.stream().noneMatch(f -> f.getXpos() == x && f.getYpos() == y)) {
				Main.fields[x][y].setGraphic(new ImageView(Main.image_floor));
			}
		})).play();
	}

	public Pair getRandomFreePosition()
	// finds a random new position which is not wall
	// and not occupied by other players
	{
		int x = 1;
		int y = 1;
		boolean found = false;
		while (!found) {
			Random r = new Random();
			x = Math.abs(r.nextInt() % 18) + 1;
			y = Math.abs(r.nextInt() % 18) + 1;
			if (Main.getBoard()[y].charAt(x) == ' ') {
				found = true;
				for (Player p : players) {
					if (p.getXpos() == x && p.getYpos() == y)
						found = false;
				}

			}
		}
		Pair p = new Pair(x, y);
		return p;
	}

	public void movePlayerOnScreen(int oldx, int oldy, int newx, int newy, String direction) {

	}

	private void removePlayer(String name) {
		for(int i = 0; i < players.size(); i++) {
			if(players.get(i).getName().equals(name)) {
				players.remove(i);
			}
		}
	}

	public void updateScoreTable() {
		Platform.runLater(() -> {
			Main.scoreList.setText(getScoreList());
		});
	}

	public void playerMoved(Player p, int newX, int newY, String direction) {
		int x = p.getXpos(), y = p.getYpos();
		p.setDirection(direction);
		updateScoreTable();
		Platform.runLater(() -> {
			Main.fields[x][y].setGraphic(new ImageView(Main.image_floor));
		});
		p.setXpos(newX);
		p.setYpos(newY);
		Platform.runLater(() -> {
			if (me.getName().equals(p.getName())) {
				for (Player player : players) {
					if (!me.getName().equals(player.getName()) && (Math.abs(me.getXpos() - player.getXpos()) > 3
							|| Math.abs(me.getYpos() - player.getYpos()) > 3)) {
						Main.fields[player.getXpos()][player.getYpos()].setGraphic(new ImageView(Main.image_floor));
					} else if (!me.getName().equals(player.getName()) && (Math.abs(me.getXpos() - player.getXpos()) <= 3
							|| Math.abs(me.getYpos() - player.getYpos()) <= 3)) {
						setHeroImage(player.getDirection(), player.getXpos(), player.getYpos());
					}
				}

			}
			if (!me.getName().equals(p.getName())
					&& (Math.abs(me.getXpos() - p.getXpos()) > 3 || Math.abs(me.getYpos() - p.getYpos()) > 3)) {
				Main.fields[newX][newY].setGraphic(new ImageView(Main.image_floor));
			} else {
				setHeroImage(direction, newX, newY);
			}

		});
	}

	private void setHeroImage(String direction, int newX, int newY) {
		if (direction.equals("right")) {
			Main.fields[newX][newY].setGraphic(new ImageView(Main.hero_right));
		} else if (direction.equals("left")) {
			Main.fields[newX][newY].setGraphic(new ImageView(Main.hero_left));
		} else if (direction.equals("up")) {
			Main.fields[newX][newY].setGraphic(new ImageView(Main.hero_up));
		} else if (direction.equals("down")) {
			Main.fields[newX][newY].setGraphic(new ImageView(Main.hero_down));
		}
	}

	public String getScoreList() {
		StringBuffer b = new StringBuffer(100);
		for (Player p : players) {
			b.append(p.getName() +"\t"+p.getPoint() + "\r\n");
		}
		return b.toString();
	}

	private String removePlayerFromScoreList(String name) {
		StringBuffer b = new StringBuffer(100);
		for (Player player : players) {
			if (!name.equals(player.getName())) {
				b.append(player.getName() +"\t"+player.getPoint() + "\r\n");
			}
		}
		return b.toString();

	}

	public Player getPlayerAt(int x, int y) {
		for (Player p : players) {
			if (p.getXpos() == x && p.getYpos() == y) {
				return p;
			}
		}
		return null;
	}
}
