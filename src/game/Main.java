package game;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;
import java.util.*;
import java.util.List;

import javax.swing.JOptionPane;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.geometry.Insets;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.image.*;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.text.*;

public class Main extends Application {

	private static ClientThread clientThread;
	private static DataOutputStream outToServer;
// TEST
	public static final int size = 20;
	public static final int scene_height = size * 20 + 100;
	public static final int scene_width = size * 20 + 200;

	public static Image image_floor;
	public static Image image_wall;
	public static Image hero_right, hero_left, hero_up, hero_down;
	public static Image fire_down, fire_horizontal, fire_left, fire_right, fire_up, fire_vertical, fire_wall_east,
			fire_wall_north, fire_wall_south, fire_wall_west;

	public static Player me;
	public static List<Player> players = new ArrayList<Player>();
	
	public static String ip;
	public static String name;

	public static Label[][] fields;
	public static TextArea scoreList;

	private static Socket clientSocket;

	public static String[] board = { // 20x20
			"wwwwwwwwwwwwwwwwwwww", "w        ww        w", "w w  w  www w  w  ww", "w w  w   ww w  w  ww",
			"w  w               w", "w w w w w w w  w  ww", "w w     www w  w  ww", "w w     w w w  w  ww",
			"w   w w  w  w  w   w", "w     w  w  w  w   w", "w ww ww        w  ww", "w  w w    w    w  ww",
			"w        ww w  w  ww", "w         w w  w  ww", "w        w     w  ww", "w  w              ww",
			"w  w www  w w  ww ww", "w w      ww w     ww", "w   w   ww  w      w", "wwwwwwwwwwwwwwwwwwww" };

	// TEST
	// -------------------------------------------
	// | Maze: (0,0) | Score: (1,0) |
	// |-----------------------------------------|
	// | boardGrid (0,1) | scorelist |
	// | | (1,1) |
	// -------------------------------------------

	public static String[] getBoard() {
		return board;
	}

	@Override
	public void start(Stage primaryStage) {
		try {
			GridPane grid = new GridPane();
			grid.setHgap(10);
			grid.setVgap(10);
			grid.setPadding(new Insets(0, 10, 0, 10));

			Text mazeLabel = new Text("Maze:");
			mazeLabel.setFont(Font.font("Arial", FontWeight.BOLD, 20));

			Text scoreLabel = new Text("Score:");
			scoreLabel.setFont(Font.font("Arial", FontWeight.BOLD, 20));

			scoreList = new TextArea();

			GridPane boardGrid = new GridPane();

			image_wall = new Image(getClass().getResourceAsStream("Image/wall4.png"), size, size, false, false);
			image_floor = new Image(getClass().getResourceAsStream("Image/floor1.png"), size, size, false, false);

			hero_right = new Image(getClass().getResourceAsStream("Image/heroRight.png"), size, size, false, false);
			hero_left = new Image(getClass().getResourceAsStream("Image/heroLeft.png"), size, size, false, false);
			hero_up = new Image(getClass().getResourceAsStream("Image/heroUp.png"), size, size, false, false);
			hero_down = new Image(getClass().getResourceAsStream("Image/heroDown.png"), size, size, false, false);

			fire_down = new Image(getClass().getResourceAsStream("Image/fireDown.png"), size, size, false, false);
			fire_horizontal = new Image(getClass().getResourceAsStream("Image/fireHorizontal.png"), size, size, false, false);
			fire_left = new Image(getClass().getResourceAsStream("Image/fireLeft.png"), size, size, false, false);
			fire_right = new Image(getClass().getResourceAsStream("Image/fireRight.png"), size, size, false, false);
			fire_up = new Image(getClass().getResourceAsStream("Image/fireUp.png"), size, size, false, false);
			fire_vertical = new Image(getClass().getResourceAsStream("Image/fireVertical.png"), size, size, false, false);
			fire_wall_east = new Image(getClass().getResourceAsStream("Image/fireWallEast.png"), size, size, false, false);
			fire_wall_north = new Image(getClass().getResourceAsStream("Image/fireWallNorth.png"), size, size, false, false);
			fire_wall_south = new Image(getClass().getResourceAsStream("Image/fireWallSouth.png"), size, size, false, false);
			fire_wall_west = new Image(getClass().getResourceAsStream("Image/fireWallWest.png"), size, size, false, false);

			fields = new Label[20][20];
			for (int j = 0; j < 20; j++) {
				for (int i = 0; i < 20; i++) {
					switch (board[j].charAt(i)) {
					case 'w':
						fields[i][j] = new Label("", new ImageView(image_wall));
						break;
					case ' ':
						fields[i][j] = new Label("", new ImageView(image_floor));
						break;
					default:
						throw new Exception("Illegal field value: " + board[j].charAt(i));
					}
					boardGrid.add(fields[i][j], i, j);
				}
			}
			scoreList.setEditable(false);

			grid.add(mazeLabel, 0, 0);
			grid.add(scoreLabel, 1, 0);
			grid.add(boardGrid, 0, 1);
			grid.add(scoreList, 1, 1);

			Scene scene = new Scene(grid, scene_width, scene_height);
			primaryStage.setScene(scene);
			primaryStage.show();

			scene.addEventFilter(KeyEvent.KEY_PRESSED, event -> {
				switch (event.getCode()) {
				case UP:    playerMoved("up");    break;
				case DOWN:  playerMoved("down");  break;
				case LEFT:  playerMoved("left");  break;
				case RIGHT: playerMoved("right"); break;
				case SPACE: fireShot(); break;
				case ESCAPE: closePlayer(); break;
				default: break;
				}
			});

//			p=getRandomFreePosition();
//			Player harry = new Player("Harry",p.getX(),p.getY(),"up");
//			players.add(harry);
//			fields[p.getX()][p.getY()].setGraphic(new ImageView(hero_up));

			outToServer = new DataOutputStream(clientSocket.getOutputStream());
			outToServer.writeBytes("hi;"+name + '\n');

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void fireShot() {
		try {
			outToServer.writeBytes("fire" + '\n');
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void playerMoved(String di) {
		try {
			outToServer.writeBytes("request;" + di + '\n');
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void sendMessage(String str) {
		try {
			outToServer.writeBytes(str + '\n');
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private void closePlayer() {
		try {
			outToServer.writeBytes("remove;" +"\n");
			System.exit(0);
		}catch(IOException e) {
			e.printStackTrace();
		}
	}

	public static void main(String[] args) throws IOException {
		try {
			ip = JOptionPane.showInputDialog("Insert ip you want to connect");
			name = JOptionPane.showInputDialog("Insert your name");
			
			clientSocket = new Socket(ip,6789);
			clientThread = new ClientThread(clientSocket, players, me);
			clientThread.start();

			launch();

		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}
}
