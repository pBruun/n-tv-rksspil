package game;

public class Player {
	private String name;
	private Pair pair;
	private int point;
	private String direction;

	public Player(String name, int xpos, int ypos, String direction) {
		this.name = name;
		this.pair = new Pair(xpos,ypos);
		this.direction = direction;
		this.point = 0;
	}

	public Player(String name, int xpos, int ypos, int point, String direction) {
		this.name = name;
		this.pair = new Pair(xpos,ypos);
		this.direction = direction;
		this.point = point;
	}
	
	public int getPoint() {
		return point;
	}
	
	public void setPair(Pair pair) {
		this.pair = pair;
	}
	
	public void setPoint(int point) {
		this.point = point;
	}
	
	public Pair getPair() {
		return pair;
	}

	public int getXpos() {
		return pair.getX();
	}
	public void setXpos(int xpos) {
		pair.setX(xpos);;
	}
	public int getYpos() {
		return pair.getY();
	}
	public void setYpos(int ypos) {
		pair.setY(ypos);
	}
	public String getDirection() {
		return direction;
	}
	public void setDirection(String direction) {
		this.direction = direction;
	}
	public void addPoints(int p) {
		point+=p;
	}
	
	public String getName() {
		return this.name;
	}
	public String toString() {
		return name+"!"+pair.getX()+"!"+pair.getY()+"!"+point+"!"+direction;
	}
}
