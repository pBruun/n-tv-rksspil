package sock;

import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.HashSet;
import java.util.Random;
import java.util.Set;

import game.Main;
import game.Pair;
import game.Player;

public class Common {
	private String tekst;
	private Set<Player> players;
	private Set<Socket> sockets;
	private boolean shot = false;
	
	public Common(String tekst) {
		super();
		this.tekst = tekst;
		players = new HashSet<>();
		sockets = new HashSet<>();
	}

	public String getTekst() {
		return tekst;
	}

	public void setTekst(String tekst) {
		this.tekst = tekst;
	}
	
	public void addSocket(Socket s) {
		sockets.add(s);
	}
	public void removeSocket(Socket s) {
		if(sockets.contains(s)) {
			sockets.remove(s);
		}
	}
	public Set<Socket> getSockets(){
		return new HashSet<Socket>(sockets);
	}
	
	public Pair getNewLocation() {
		boolean found = false;
		int x = 0;
		int y = 0;
		while (!found) {
			Random r = new Random();
			x = Math.abs(r.nextInt() % 18) + 1;
			y = Math.abs(r.nextInt() % 18) + 1;
			if (Main.getBoard()[y].charAt(x) == ' ') {
				found = true;
				for (Player p : players) {
					if (p.getXpos() == x && p.getYpos() == y)
						found = false;
				}

			}
		}
		return new Pair(x, y);
	}

	public Player createPlayer(String name) {
		boolean found = false;
		int x = 0;
		int y = 0;
		while (!found) {
			Random r = new Random();
			x = Math.abs(r.nextInt() % 18) + 1;
			y = Math.abs(r.nextInt() % 18) + 1;
			if (Main.getBoard()[y].charAt(x) == ' ') {
				found = true;
				for (Player p : players) {
					if (p.getXpos() == x && p.getYpos() == y)
						found = false;
				}

			}
		}
		Player p = new Player(name, x, y, "up");
		players.add(p);
				
		return p;

	}
	public Player removePlayer(Player p) {
		if(players.contains(p)) {
		players.remove(p);
		
		return p;
		}else {
			return null;
		}
	}
	public Set<Player> getPlayers(){
		return new HashSet<Player>(players);
	}
	
	public boolean isShot() {
		return this.shot;
	}
	
	public void setShot(boolean val) {
		this.shot = val;
	}
	
	public synchronized void sendToAll(String msg) {
		for (Socket s : sockets) {
			DataOutputStream temp;
			try {
				temp = new DataOutputStream(s.getOutputStream());
				temp.writeBytes(msg + "\n");
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	public synchronized void updatePoints(Player player, Player hit) {
		hit.setPair(getNewLocation());
		hit.addPoints(-5);
		player.addPoints(10);
	}

}
