package sock;

import java.net.*;
import java.util.Random;
import java.util.Set;

import game.Main;
import game.Pair;
import game.Player;
import javafx.application.Platform;
import javafx.scene.image.ImageView;

import java.io.*;

public class ServerThread extends Thread {
	private Socket connSocket;
	private Common c;
	private Player player;

	public ServerThread(Socket connSocket, Common c) {
		this.connSocket = connSocket;
		this.c = c;
		c.addSocket(connSocket);
	}

	public void run() {
		boolean run = true;
		while (run) {
			try {
				BufferedReader inFromClient = new BufferedReader(new InputStreamReader(connSocket.getInputStream()));
				DataOutputStream outToClient = new DataOutputStream(connSocket.getOutputStream());
				String input = inFromClient.readLine();
				if (input.startsWith("hi")) {
					String[] name = input.split(";");
					player = c.createPlayer(name[1]);
					outToClient.writeBytes("ack;");
					String allPlayers = "";
					for (Player p : c.getPlayers()) {
						allPlayers += p.toString() + ":";
					}

					c.sendToAll("newplayer;" + player.toString() + ";" + allPlayers);
				} else if (input.startsWith("bai")) {
					c.removePlayer(player);
					c.removeSocket(connSocket);
					outToClient.writeBytes("ack" + "\n");
					c.sendToAll(input);
					connSocket.close();
				} else if (input.startsWith("request;")) {
					String move = input.split(";")[1];
					player.setDirection(move);
					int xPos = player.getXpos();
					int yPos = player.getYpos();

					if (move.equals("up")) {
						yPos -= 1;
						if (!checkWallLocation(xPos, yPos)) {
							if (!checkPlayerLocation(xPos, yPos)) {
								player.setYpos(yPos);
							}
						}

					} else if (move.equals("down")) {
						yPos += 1;
						if (!checkWallLocation(xPos, yPos)) {
							if (!checkPlayerLocation(xPos, yPos)) {
								player.setYpos(yPos);
							}
						}
					} else if (move.equals("left")) {
						xPos -= 1;
						if (!checkWallLocation(xPos, yPos)) {
							if (!checkPlayerLocation(xPos, yPos)) {
								player.setXpos(xPos);
							}
						}

					} else {
						xPos += 1;
						if (!checkWallLocation(xPos, yPos)) {
							if (!checkPlayerLocation(xPos, yPos)) {
								player.setXpos(xPos);
							}
						}
					}
					c.sendToAll("playermove;" + player.toString());
				} else if (input.startsWith("fire")) {
					// if (!c.isShot()) {
					// c.setShot(true);
					String[] board = Main.getBoard();
					c.sendToAll("shot;" + player.toString());
					int x_move =  0 ;
					int y_move =  0 ;
					switch (player.getDirection()) {
					case "left":
						x_move = -1;
						y_move = 0;
						break;
					case "right":
						x_move = 1;
						y_move = 0;
						break;
					case "up":
						x_move = 0;
						y_move = -1;
						break;
					case "down":
						x_move = 0;
						y_move = 1;
						break;
					default:
						break;
					}
					int[] x = {player.getXpos()}, y = {player.getYpos()};
					boolean finished = false;
					Player hit = null;
					while (!finished) {
						if (board[y[0] + y_move].charAt(x[0] + x_move) == 'w') {
							finished = true;
						} else if (c.getPlayers().stream().anyMatch(
								f -> !f.getName().equals(player.getName()) && f.getXpos() == x[0] && f.getYpos() == y[0])) {
							finished = true;
							hit = c.getPlayers().stream().filter(
									f -> !f.getName().equals(player.getName()) && f.getXpos() == x[0] && f.getYpos() == y[0]).findAny().orElse(null);
						} else {
							y[0] = y[0] + y_move;
							x[0] = x[0] + x_move;
						}
					}
					if(hit != null) {
						c.updatePoints(player, hit);
						c.sendToAll("playermove;" + hit.toString());
					}
					// outToClient.writeBytes("ack\n");
					// } else {
					// outToClient.writeBytes("nack\n");
					// }

				} else if (input.startsWith("remove;")) {
					c.sendToAll("playerremoved;" + player.toString());
					c.removePlayer(player);
					c.removeSocket(connSocket);
					run = false;
					connSocket.close();
				}

			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		// do the work here
	}

	private boolean checkPlayerLocation(int xPos, int yPos) {
		boolean playerAlreadyThere = false;
		Set<Player> players = c.getPlayers();
		for (Player p : players) {
			if (p.getXpos() == xPos && p.getYpos() == yPos) {
				playerAlreadyThere = true;
			}
		}
		return playerAlreadyThere;
	}

	private boolean checkWallLocation(int xPos, int yPos) {
		boolean wall = false;
		String[] board = Main.getBoard();
		if (board[yPos].charAt(xPos) == 'w') {
			wall = true;
		}
		return wall;
	}

	public Player getPlayerAt(int x, int y) {
		for (Player p : c.getPlayers()) {
			if (p.getXpos() == x && p.getYpos() == y) {
				return p;
			}
		}
		return null;
	}

}
